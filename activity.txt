1) Find users with letter 's' in their first name or 'd' in their last name.
db.users.find(
 { $or: [ 
   { firstName: {$regex: 's', $options: '$i' }},
   { lastName: {$regex: 'd', $options: '$i' }}
   ]
  }
)

2) Show only the firstName and lastName fields and hide 
their _id field.
db.users.find(
  {
    firstName: 1,
    lastName: 1,
    _id: 0
  }
)

3) Find users who are from the HR department and their age is
 greater then or equal to 70.
db.users.find( { $and: [ { department: "HR" }, { age: { $gte: 70} }] } )


4) Find users with the letter 'e' in their first name 
and has an age of less than or equal to 30.
db.users.find( { $and: [ { firstName: { $regex: 'e', $options: '$i' }}, { age: { $lte: 30 } } ]})


5) Display all records.
db.users.find()


6) Briefly explain the code below:
db.users.find({
  $or: [
    {firstName: { $regex: 'a', $options: '$i' }},
    {lastName: { $regex: 'a', $options: '$i' }}
  ]
}, { firstName: 1, lastName: 1, _id: 0 })
> Searches for users that has the letter 'a' on their first and last names
discarding capitalizations. 
> It also displays firstName, lastName, but does not show the _id field.


7) What is the output of the code above?
> Only displayed firstName, lastName, and hides the _id field of 
users who has letter 'a' on their first and last names.

8) For numbers 8 and 9, briefly explain the code below:
 db.users.find({
   $and: [
     { isAdmin: true },
     { isActive: true }
   ]
 })
> Displays the users that both have 'true' on their isAdmin and isActive fields.

 9) db.courses.find({
   $and: [
     { name: { $regex: 'u', $options: '$i' }},
     { price: { $gte: 13000 }}
   ]
 })
 > Displays if both the courses that contains letter 'u' in their name 
 and has a price of greater than or equal to 13000.

 10) Display all records in courses.
 db.courses.find() (idk? - what does 'records' mean?)